from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import random
import bcrypt
import os
 
from models import Db, User, Family, FamilyMember, PhoneNumberType, PhoneNumber
 
from faker import Faker

# >>> import bcrypt
# >>> salt = bcrypt.gensalt()
# >>> hashed = bcrypt.hashpw('secret', salt)
# >>> hashed.find(salt)
# 0
# >>> hashed == bcrypt.hashpw('secret', hashed)
# True
# >>>

try:
    os.remove('phonebook.db')
except Exception as e:
    pass

engine = create_engine('sqlite:///phonebook.db')
# Bind the engine to the metadata of the Base class so that the
# declaratives can be accessed through a DBSession instance
Db.metadata.create_all(engine)
Db.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
# A DBSession() instance establishes all conversations with the dataDb
# and represents a "staging zone" for all the objects loaded into the
# database session object. Any change made against the objects in the
# session won't be persisted into the database until you call
# session.commit(). If you're not happy about the changes, you can
# revert all of them back to the last commit by calling
# session.rollback()
session = DBSession()

def generate_fakes(session):
    fake = Faker('en_CA')

    # Users to be added:
    user = User(username="admin", password=bcrypt.hashpw("password".encode('utf8'), bcrypt.gensalt()))
    session.add(user)
    session.commit()

    pnt_ids = []
    acodes = ["306", "403", "587"]

    for pntype in ['home', 'work', 'mobile']:
        phone_number_type = PhoneNumberType(type=pntype)
        session.add(phone_number_type)
        session.commit()

        pnt_ids.append(phone_number_type.id)

    birth_years = [None for _ in range(10)]+[year for year in range(1950,2018+1)]
    middle_initials = [None for _ in range(5)]+"A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,U,R,S,T,U,V,W,X,Y,Z".split(",")

    for _ in range(10):
        family = Family(
            name=fake.last_name(), 
            address_1=fake.street_address(), 
            city=fake.city(), 
            province=fake.province_abbr(), 
            postal_code=fake.postalcode(),
            user_id=user.id)
        session.add(family)
        session.commit()

        print(f"{family.name}: {family.id}")
        
        for _ in range(random.randint(1,4)):
            member_birth_month = random.randint(1,12)
            if member_birth_month == 2:
                member_birth_day = random.randint(1,28)
            elif member_birth_month in [9,4,6,11]:
                member_birth_day = random.randint(1,30)
            else:
                member_birth_day = random.randint(1,31)

            family_member = FamilyMember(
                first_name=fake.first_name(),
                middle_name=middle_initials[random.randint(0,len(middle_initials)-1)],
                last_name=family.name,
                family_id=family.id,
                user_id=user.id,
                birth_year=birth_years[random.randint(0,len(birth_years)-1)],
                birth_month=member_birth_month,
                birth_day=member_birth_day,
            )
            session.add(family_member)
            session.commit()

            print(f"  {family_member.first_name} {family_member.middle_name} {family_member.last_name}")

            for _ in range(random.randint(0,2)):
                phone_number = PhoneNumber(
                    country_code="+1",
                    area_code=acodes[random.randint(0,len(acodes) -1)],
                    digits=''.join(["%s" % random.randint(0, 9) for num in range(0, 7)]),
                    phone_number_type_id=pnt_ids[random.randint(0, len(pnt_ids)-1)],
                    family_member_id=family_member.id,
                    user_id=user.id
                )
                session.add(phone_number)
                session.commit()

                print(f"    phone_type: {phone_number.phone_number_type_id}, {phone_number.country_code}-{phone_number.area_code}-{phone_number.digits[:3]}-{phone_number.digits[3:]}")

    print("""==================================
    Fake Families Generated""")

if __name__ == '__main__':
    generate_fakes(session)