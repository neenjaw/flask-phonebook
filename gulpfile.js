var path = require('path');
var gulp = require('gulp');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');
var merge = require('merge-stream');
var handlebars = require('gulp-handlebars');

var paths = {
    handlebars: {
        templates: 'hbs-templates/**/[^_]*.hbs',
        partials: 'hbs-templates/partials/_*.hbs',
        destination: 'static/js/',
        output: 'templates.js'
    }
};

function templates() {
    // Assume all partials start with an underscore
    // You could also put them in a folder such as source/templates/partials/*.hbs
    var partials = gulp.src([paths.handlebars.partials])
        .pipe(handlebars())
        .pipe(wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
            imports: {
                processPartialName: function (fileName) {
                    // Strip the extension and the underscore
                    // Escape the output with JSON.stringify
                    return JSON.stringify(path.basename(fileName, '.js').substr(1));
                }
            }
        }));

    var templates = gulp.src(paths.handlebars.templates)
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            namespace: 'Phonebook.templates',
            noRedeclare: true // Avoid duplicate declarations
        }));

    // Output both the partials and the templates as build/js/templates.js
    return merge(partials, templates)
        .pipe(concat(paths.handlebars.output))
        .pipe(gulp.dest(paths.handlebars.destination));
}

function watch() {
    gulp.watch(paths.handlebars.templates, templates);
    gulp.watch(paths.handlebars.partials, templates);
}

gulp.task('templates', templates);

gulp.task('default', gulp.series(templates, watch));