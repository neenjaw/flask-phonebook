#!/bin/sh

# Python environment setup
virtualenv -p python3 ./env
source ./env/bin/activate
pip install flask flask_sqlalchemy sqlalchemy bcrypt faker

# NodeJS environment setup
npm install -g gulp-cli
npm install

# compile the templates
gulp templates

# initialize the DB
python init_db.py

# start the server
python project.py