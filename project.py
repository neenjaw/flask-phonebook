import string
import random
import time
import json
import re
import calendar
from datetime import date

from pprint import pformat, pprint

from flask import make_response
from flask import session as login_session
from flask import Flask, render_template, request, redirect, jsonify, url_for, flash

from models import Db, User, Family, FamilyMember, PhoneNumberType, PhoneNumber
from sqlalchemy import asc

from sqlite3 import IntegrityError

app = Flask(__name__)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///phonebook.db"

Db.init_app(app)

@app.context_processor
def inject_now():
    def current_age(birth_year, birth_month, birth_day):
        age = date.today()-date(birth_year,birth_month,birth_day)
        return (age.days // 365)    

    timestring = str(time.time())
    index_of_dot = timestring.find(".")

    return {
        'now': timestring[:index_of_dot]+timestring[(index_of_dot+1):],
        'months_table': [{"num": nu, "abbr": a, "name": na} for (nu, a, na) in zip(range(1, 13), calendar.month_abbr[1:], calendar.month_name[1:])],
        'years_table': [year for year in range(date.today().year, date.today().year - 110, -1)],
        'calculate_age': current_age,
        }


@app.route('/')
@app.route('/family/')
def show_family():
    families = Family.query.order_by(asc(Family.name)).all()
    
    alpha_pagination = sorted({family.name[0].upper() for family in families})

    return render_template('index.html', families=families, alpha_pagination=alpha_pagination)

@app.route('/family/new/', methods=['GET', 'POST'])
def new_family():
#     if 'username' not in login_session:
#         return redirect('/login')

    phone_types = PhoneNumberType.query.all()

    if request.method == 'GET':
        return render_template('newFamily.html', PHONE_TYPES=[p.serialize for p in phone_types])
    elif request.method == 'POST':
        # process the form
        [family, members] = process_new_family_form(request.form)
        
        # create Family object
        newFamily = Family(
            name=family["name"],
            address_1=family["address-1"] if family["address-1"].strip() != "" else None,
            address_2=family["address-2"] if family["address-2"].strip() != "" else None,
            address_3=family["address-3"] if family["address-3"].strip() != "" else None,
            city=family["city"] if family["city"].strip() != "" else None,
            province=family["province"] if family["province"].strip() != "" else None,
            postal_code=family["postal-code"] if family["postal-code"].strip() != "" else None,
            user_id=login_session["user_id"] if "user_id" in login_session else 1
        )
        Db.session.add(newFamily)
        Db.session.flush()

        # create each FamilyMember
        for member in members.values():
            newFamilyMember = FamilyMember(
                family_id=newFamily.id,
                first_name=member['first-name'],
                middle_name=member['middle-name'] if "middle-name" in member else None,
                last_name=member['last-name'],
                birth_year=int(member['birthday-year']) if member['birthday-year'] != "no" else None,
                birth_month=int(member['birthday-month']),
                birth_day=int(member['birthday-date']), 
                user_id=login_session["user_id"] if "user_id" in login_session else 1
            )
            Db.session.add(newFamilyMember)
            Db.session.flush()

            # if the member has phone numbers to add, create PhoneNumber
            if "phone_numbers" in member:
                for phone_number in member["phone_numbers"].values():
                    if len(phone_number['number']) < 10:
                        continue
                    elif len(phone_number['number']) == 10:
                        country_code = "+1"
                    else:
                        country_code = "+" + phone_number['number'][:-10]

                    area_code = phone_number['number'][-10:-7]
                    digits = phone_number['number'][-7:]

                    newPhoneNumber = PhoneNumber(
                        country_code=country_code,
                        area_code=area_code,
                        digits=digits,
                        phone_number_type_id=phone_number['type'],
                        family_member_id=newFamilyMember.id,
                        user_id=login_session["user_id"] if "user_id" in login_session else 1
                    )
                    Db.session.add(newPhoneNumber)

        Db.session.commit()
        flash(f"{newFamily.name} added to the directory!")
        return redirect(url_for('show_family'))
    else:
        raise Exception

def process_new_family_form(form_data):
    # compile the regex's for processing the data
    re_family = re.compile("^family-(.+)$")
    re_member = re.compile("^member-([0-9]+)_(.+)$")
    re_member_phone = re.compile("^phone-number-([0-9]+)_(.+)$")
    re_format_phone = re.compile("\D")

    family = {}
    members = {}

    # loop through each of the form's keys in the dictionary
    for key, value in form_data.items():
        # check if the key matches family data
        match = re_family.match(key)
        if match:
            family[match.group(1)] = value
            continue

        # check if the key matches member data
        match = re_member.match(key)
        if match:
            member_id = match.group(1)
            member_attribute = match.group(2)

            if member_id not in members:
                members[member_id] = {}

            # check if the member key also matches phone data
            match = re_member_phone.match(member_attribute)
            if match:
                phone_id = match.group(1)
                phone_attribute = match.group(2)

                if "phone_numbers" not in members[member_id]:
                    members[member_id]["phone_numbers"] = {}

                if phone_id not in members[member_id]["phone_numbers"]:
                    members[member_id]["phone_numbers"][phone_id] = {}

                if phone_attribute == "number":
                    save_value = re_format_phone.sub("", value)
                else:
                    save_value = value

                members[member_id]["phone_numbers"][phone_id][phone_attribute] = save_value
            else:
                members[member_id][member_attribute] = value

            continue

    return [family, members]

@app.route('/family/edit/')
def show_edit_family():
    families = Family.query.order_by(asc(Family.name)).all()
    alpha_pagination = sorted({family.name[0].upper() for family in families})

    return render_template('indexEditFamily.html', families=families, alpha_pagination=alpha_pagination)

@app.route('/family/<int:family_id>/edit/', methods=['GET', 'UPDATE', 'DELETE'])
def edit_family(family_id):
    if request.method == 'GET':
        family = Family.query.filter_by(id=family_id).one()

        return render_template('edit_family.html', family=family)
    elif request.method == 'UPDATE':
        pass
    elif request.method == 'DELETE':
        response_data = {}

        try:
            family = Family.query.filter_by(id=family_id).one()
            Db.session.delete(family)
            Db.session.commit()

            status_code = 200
            response_data["meta"] = {}
        except IntegrityError as e:
            status_code = 500
            response_data["errors"] = []
            response_data["errors"].append({
                "status":status_code,
                "title":"Server Error",
                "detail":"Server encountered an error",
                })
        except Exception as e:
            status_code = 404
            response_data["errors"] = []
            response_data["errors"].append({
                "status":status_code,
                "title":"Invalid Id",
                "detail":"Family not found for deletion",
                })
        finally:
            return jsonify(response_data), status_code
    else:
        raise Exception

@app.route('/family/<int:family_id>/member/new/', methods=['GET', 'POST'])
def new_member(family_id):
    if request.method == 'GET':
        pass
    elif request.method == 'POST':
        pass
    else:
        raise Exception

@app.route('/member/<int:member_id>/edit/', methods=['GET', 'UPDATE', 'DELETE'])
@app.route('/family/<int:family_id>/member/<int:member_id>/edit/', methods=['GET', 'UPDATE', 'DELETE'])
def edit_member(member_id, family_id=None):
    if request.method == 'GET':
        member = FamilyMember.query.filter_by(id=member_id).one()

        return render_template('edit_member.html', member=member)
    elif request.method == 'UPDATE':
        pass
    elif request.method == 'DELETE':
        response_data = {}

        try:
            member = FamilyMember.query.filter_by(id=member_id).one()
            Db.session.delete(member)
            Db.session.commit()

            status_code = 200
            response_data["meta"] = {}
        except IntegrityError as e:
            status_code = 500
            response_data["errors"] = []
            response_data["errors"].append({
                "status":status_code,
                "title":"Server Error",
                "detail":"Server encountered an error",
                })
        except Exception as e:
            status_code = 404
            response_data["errors"] = []
            response_data["errors"].append({
                "status":status_code,
                "title":"Invalid Id",
                "detail":"Family Member not found for deletion",
                })
        finally:
            return jsonify(response_data), status_code
    else:
        raise Exception

# @app.route('/login', methods=['GET', 'POST'])
# def showLogin():
#     if request.method == 'POST':
#         doLogin()        
#     else:
#         state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
#         login_session['state'] = state

#         return render_template('login.html', STATE=state)

# def doLogin():
#     if request.args.get('state') != login_session['state']:
#         response = make_response(json.dumps('Invalid state parameters'), 401)
#         response.headers['Content-Type'] = 'application/json'
#         return response

#     username = request.args.get('password')
#     rawPassword = request.args.get('password')

#     # Store the access token in the session for later use.
#     login_session['username'] = data['name']
#     login_session['picture'] = data['picture']
#     login_session['email'] = data['email']

#     login_session['user_id'] = get_user_id(login_session['email'])

#     if login_session['user_id'] is None:
#         # print(">>> Making a new user")
#         login_session['user_id'] = create_user(login_session)

#     output = """
#         <h1>Welcome, {}!</h1>
#         <img src="{}" style="width: 300px; height: 300px; border-radius: 150px; -webkit-border-radius: 150px; -moz-border-radius: 150px;">
#     """.format(login_session['username'], login_session['picture'])

#     flash("you are now logged in as %s" % login_session['username'])

#     return output

# @app.route('/logout')
# def logout():
#     del login_session['username']

#     response = make_response(json.dumps("Successfuly delete."), 200)
#     response.headers['Content-Type'] = 'application/json'
#     return response

# # JSON APIs to view Restaurant Information
# @app.route('/restaurant/<int:restaurant_id>/menu/JSON')
# def restaurantMenuJSON(restaurant_id):
#     restaurant = Restaurant,query.filter_by(id=restaurant_id).one()
#     items = MenuItem.query.filter_by(
#         restaurant_id=restaurant_id).all()
#     return jsonify(MenuItems=[i.serialize for i in items])

# @app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/JSON')
# def menuItemJSON(restaurant_id, menu_id):
#     Menu_Item = MenuItem.query.filter_by(id=menu_id).one()
#     return jsonify(Menu_Item=Menu_Item.serialize)


# @app.route('/restaurant/JSON')
# def restaurantsJSON():
#     restaurants = Restaurant.query.all()
#     return jsonify(restaurants=[r.serialize for r in restaurants])

# # Edit a restaurant
# @app.route('/restaurant/<int:restaurant_id>/edit/', methods=['GET', 'POST'])
# def editRestaurant(restaurant_id):
#     if 'username' not in login_session:
#         return redirect('/login')

#     editedRestaurant = Restaurant.query.filter_by(id=restaurant_id).one()
        
#     # Get the user associated with the restaurant
#     editedRestaurantOwner = User.query.filter_by(id=editedRestaurant.user_id).one()
#     # check the current user against the restaurant's user, if no match, then redirect
#     if login_session['email'] != editedRestaurantOwner.email:
#         flash('You are not authorized to edit this restaurant %s' % editedRestaurant.name)
#         return redirect(url_for('showRestaurants', restaurant_id=restaurant_id))

#     if request.method == 'POST':
#         if request.form['name']:
#             editedRestaurant.name = request.form['name']
#             Db.session.commit()
#             flash('Restaurant Successfully Edited %s' % editedRestaurant.name)
#             return redirect(url_for('showRestaurants'))
#     else:
#         return render_template('editRestaurant.html', restaurant=editedRestaurant)


# # Delete a restaurant
# @app.route('/restaurant/<int:restaurant_id>/delete/', methods=['GET', 'POST'])
# def deleteRestaurant(restaurant_id):
#     if 'username' not in login_session:
#         return redirect('/login')
        
#     restaurantToDelete = Restaurant.query.filter_by(id=restaurant_id).one()

#     # Get the user associated with the restaurant
#     restaurantToDeleteOwner = User.query.filter_by(id=restaurantToDelete.user_id).one()
#     # check the current user against the restaurant's user, if no match, then redirect
#     if login_session['email'] != restaurantToDeleteOwner.email:
#         flash('You are not authorized to delete this restaurant %s' % restaurantToDelete.name)
#         return redirect(url_for('showRestaurants', restaurant_id=restaurant_id))

#     if request.method == 'POST':
#         Db.session.delete(restaurantToDelete)
#         Db.session.commit()
#         flash('%s Successfully Deleted' % restaurantToDelete.name)
#         return redirect(url_for('showRestaurants', restaurant_id=restaurant_id))
#     else:
#         return render_template('deleteRestaurant.html', restaurant=restaurantToDelete)

# # Show a restaurant menu
# @app.route('/restaurant/<int:restaurant_id>/')
# @app.route('/restaurant/<int:restaurant_id>/menu/')
# def showMenu(restaurant_id):
#     restaurant = Restaurant.query.filter_by(id=restaurant_id).one()
#     items = MenuItem.query.filter_by(restaurant_id=restaurant_id).all()
#     restaurantOwner = User.query.filter_by(id=restaurant.user_id).one()

#     if 'username' in login_session:
#         if login_session['email'] == restaurantOwner.email:
#             return render_template('menu.html', items=items, restaurant=restaurant, creator=restaurantOwner)

#     return render_template('publicmenu.html', items=items, restaurant=restaurant, creator=restaurantOwner)

# # Create a new menu item
# @app.route('/restaurant/<int:restaurant_id>/menu/new/', methods=['GET', 'POST'])
# def newMenuItem(restaurant_id):
#     if 'username' not in login_session:
#         return redirect('/login')

#     restaurant = Restaurant.query.filter_by(id=restaurant_id).one()
    
#     # Get the user associated with the restaurant
#     restaurantOwner = User.query.filter_by(id=restaurant.user_id).one()
#     # check the current user against the restaurant's user, if no match, then redirect
#     if login_session['email'] != restaurantOwner.email:
#         flash('You are not authorized to delete items from \'%s\'s menu' % restaurant.name)
#         return redirect(url_for('showMenu', restaurant_id=restaurant_id))
        
#     if request.method == 'POST':
#         newItem = MenuItem(name=request.form['name'], description=request.form['description'],
#                            price=request.form['price'], course=request.form['course'], restaurant_id=restaurant_id, user_id=login_session['user_id'])
#         Db.session.add(newItem)
#         Db.session.commit()
#         flash('New Menu %s Item Successfully Created' % (newItem.name))
#         return redirect(url_for('showMenu', restaurant_id=restaurant_id))
#     else:
#         return render_template('newmenuitem.html', restaurant_id=restaurant_id)

# # Edit a menu item
# @app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/edit', methods=['GET', 'POST'])
# def editMenuItem(restaurant_id, menu_id):
#     if 'username' not in login_session:
#         return redirect('/login')
        
#     editedItem = MenuItem.query.filter_by(id=menu_id).one()
#     restaurant = Restaurant.query.filter_by(id=restaurant_id).one()

#     # Get the user associated with the restaurant
#     restaurantOwner = User.query.filter_by(id=restaurant.user_id).one()
#     # check the current user against the restaurant's user, if no match, then redirect
#     if login_session['email'] != restaurantOwner.email:
#         flash('You are not authorized to edit \'%s\'s menu' % restaurant.name)
#         return redirect(url_for('showMenu', restaurant_id=restaurant_id))

#     if request.method == 'POST':
#         if request.form['name']:
#             editedItem.name = request.form['name']
#         if request.form['description']:
#             editedItem.description = request.form['description']
#         if request.form['price']:
#             editedItem.price = request.form['price']
#         if request.form['course']:
#             editedItem.course = request.form['course']
#         Db.session.add(editedItem)
#         Db.session.commit()
#         flash('Menu Item Successfully Edited')
#         return redirect(url_for('showMenu', restaurant_id=restaurant_id))
#     else:
#         return render_template('editmenuitem.html', restaurant_id=restaurant_id, menu_id=menu_id, item=editedItem)


# # Delete a menu item
# @app.route('/restaurant/<int:restaurant_id>/menu/<int:menu_id>/delete', methods=['GET', 'POST'])
# def deleteMenuItem(restaurant_id, menu_id):
#     if 'username' not in login_session:
#         return redirect('/login')
        
#     itemToDelete = MenuItem.query.filter_by(id=menu_id).one()
#     restaurant = Restaurant.query.filter_by(id=restaurant_id).one()

#     # Get the user associated with the restaurant
#     restaurantOwner = User.query.filter_by(id=restaurant.user_id)
#     # check the current user against the restaurant's user, if no match, then redirect
#     if login_session['email'] != restaurantOwner.email:
#         flash('You are not authorized to delete items from \'%s\'s menu' % restaurant.name)
#         return redirect(url_for('showMenu', restaurant_id=restaurant_id))

#     if request.method == 'POST':
#         Db.session.delete(itemToDelete)
#         Db.session.commit()
#         flash('Menu Item Successfully Deleted')
#         return redirect(url_for('showMenu', restaurant_id=restaurant_id))
#     else:
#         return render_template('deleteMenuItem.html', item=itemToDelete)

# def get_user_id(email):
#     try:
#         user = User.query.filter_by(email = email).one()
#         return user.id
#     except:
#         return None

# def getUserInfo(user_id):
#     try:
#         user = User.query.filter_by(user_id = user_id).one()
#         return user
#     except:
#         return None

# def create_user(login_session):
#     try:
#         newUser = User(
#             name=login_session['username'],
#             email=login_session['email'],
#             picture=login_session['picture']
#         )

#         Db.session.add(newUser)
#         Db.session.commit()

#         user = User.query.filter_by(email = login_session['email']).one()
#         return user.id
#     except:
#         return None

def camel_to_snake_case(str):
    return re.sub(r"[A-Z]", (lambda m: "_"+m.group(0).lower()), s)     
def snake_to_lisp_case(str):
    return re.sub(r"_", "-", str)

def lisp_to_snake_case(str):
    return re.sub(r"-", "_", str)

def snake_to_camel_case(str):
    parts = str.split("_")
    return parts[0] + "".join(x.title() for x in parts[1:])

if __name__ == '__main__':
    app.secret_key = 'super_secret_key'
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
