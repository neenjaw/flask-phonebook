from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, validates
from flask_sqlalchemy import SQLAlchemy

import calendar

Db = SQLAlchemy()

class User (Db.Model):
    __tablename__ = 'user'

    id       =   Column(Integer, primary_key=True)
    username =   Column(String(250), nullable=False, unique=True)
    password =   Column(String(250), nullable=False)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id'        : self.id,
            'name'      : self.username,
            'email'     : self.password,
        }

class Family(Db.Model):
    __tablename__ = 'family'
   
    id          = Column(Integer, primary_key=True)
    name        = Column(String(250), nullable=False)
    address_1   = Column(String(250), nullable=True)
    address_2   = Column(String(250), nullable=True)
    address_3   = Column(String(250), nullable=True)
    city        = Column(String(250), nullable=True)
    province    = Column(String(250), nullable=True)
    postal_code = Column(String(250), nullable=True)

    members     = relationship(
                    "FamilyMember", 
                    back_populates="family",
                    order_by="FamilyMember.birth_year, FamilyMember.birth_month, FamilyMember.birth_day, FamilyMember.first_name",
                    cascade="save-update, merge, delete")

    user_id     = Column(Integer, ForeignKey('user.id'), nullable=True)
    user        = relationship("User")

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id'          : self.id,
            'family_name' : self.family_name,
            'address_1'   : self.address_1,
            'address_2'   : self.address_2,
            'address_3'   : self.address_3,
            'city'        : self.city,
            'province'    : self.province,
            'postal_code' : self.postal_code,
        }
 
class FamilyMember(Db.Model):
    __tablename__ = 'family_member'

    id            = Column(Integer, primary_key = True)
    first_name    = Column(String(80), nullable = False)
    middle_name   = Column(String(80), nullable = True)
    last_name     = Column(String(80), nullable = False)

    birth_year    = Column(Integer, nullable = True)
    birth_month   = Column(Integer, nullable = False)
    birth_day     = Column(Integer, nullable = False)

    @validates('birth_month')
    def validate_birth_month(self, key, birth_month):
        assert birth_month in range(1,12+1)
        return birth_month

    @validates('birth_day')
    def validate_birth_day(self, key, birth_day):
        assert (
            # if the birthday is in february
            self.birth_month == 2 and ( 
                # if the year is known and is a leap year
                (self.birth_year != None and calendar.isleap(self.birth_year) and birth_day in range(1,29+1)) or
                # if the year is known and is not a leap year
                (self.birth_year != None and not calendar.isleap(self.birth_year) and birth_day in range(1,28+1)) or
                # if the year is unknown
                (self.birth_year == None and birth_day in range(1,29+1))
            )
        ) or (
            # if the birthmonth is a month with 30 days (apr, jun, sep, nov)
            self.birth_month in [4,6,9,11] and birth_day in range(1,30+1)
        ) or (birth_day in range(1,31+1))

        return birth_day

    phone_numbers = relationship("PhoneNumber", back_populates="family_member", cascade="save-update, merge, delete")

    family_id     = Column(Integer,ForeignKey('family.id'), nullable=False)
    family        = relationship("Family", back_populates="members")

    user_id       = Column(Integer, ForeignKey('user.id'), nullable=True)
    user          = relationship("User")

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'          : self.id,
           'first_name'  : self.first_name,
           'middle_name' : self.middle_name,
           'last_name'   : self.last_name,
           'birthday'    : f"{self.birth_year}-{self.birth_month:02}-{self.birth_day:02}" if self.birth_year is not None else f"{self.birth_month:02}-{self.birth_day:02}"
       }

class PhoneNumberType(Db.Model):
    __tablename__ = 'phone_number_type'

    id = Column(Integer, primary_key = True)    
    type = Column(String(80), nullable = False)

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'   : self.id,
           'type' : self.type,
       }

class PhoneNumber(Db.Model):
    __tablename__ = 'phone_number'

    id                   = Column(Integer, primary_key = True)
    country_code         = Column(String(2), nullable = False)
    area_code            = Column(String(3), nullable = False)
    digits               = Column(String(7), nullable = False)

    phone_number_type_id = Column(Integer,ForeignKey('phone_number_type.id'), nullable=False)
    phone_number_type    = relationship("PhoneNumberType") 
    family_member_id     = Column(Integer,ForeignKey('family_member.id'), nullable=False)
    family_member        = relationship("FamilyMember", back_populates="phone_numbers")
    user_id              = Column(Integer, ForeignKey('user.id'), nullable=True)
    user                 = relationship("User")

    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
           'id'           : self.id,
           'country_code' : self.country_code,
           'area_code'    : self.area_code,
           'digits'       : self.digits,
       }