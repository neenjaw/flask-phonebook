const EditFamily = (function(){
    function deleteFamily(familyId) {
        const url = `http://0.0.0.0:5000/family/${familyId}/edit/`;
        const body = { type: 'family', family_id: familyId };

        deleteRequest(url, body);
    }

    function deleteFamilyMember(memberId) {
        const url = `http://0.0.0.0:5000/member/${memberId}/edit/`;
        const body = { type: 'member', member_id: memberId };

        deleteRequest(url, body);
    }

    function deleteRequest(url, body) {
        const request = {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                // "Content-Type": "application/x-www-form-urlencoded",
                'Content-Type': 'application/json; charset=utf-8',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(body), // body data type must match "Content-Type" header
        };

        fetch(url, request)
            .then(res => {
                if (!res.ok) throw('Fetch request not ok');
                
                return res.json().then(data => ({status: res.status, body: data})); })
            .then(obj => {
                if (obj.status === 200) return location.reload(); })
            .catch(error => console.error(error));
        
    }

    document.addEventListener('click', (event) => {
        const target = event.target;

        if (target.classList.contains('delete') && target.dataset.deleteType) {
            event.preventDefault();

            if (target.dataset.deleteType == 'family') {
                deleteFamily(parseInt(target.dataset.familyId));
            }
            else if (target.dataset.deleteType == 'member') {
                deleteFamilyMember(parseInt(target.dataset.memberId));
            }
        }
    });

    return {
        deleteFamily,
        deleteFamilyMember
    };
})();