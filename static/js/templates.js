Handlebars.registerPartial("memberPhoneNumberEntry", Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "required";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <option value=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "</option>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"form-group row align-items-center\" data-member=\""
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "\" data-phone-number=\""
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "\">\n    <label for=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_phone-number-"
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "\" class=\"col-sm-2 col-form-label\">Contact "
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "</label>\n    <div class=\"col-sm-7\">\n        <input type=\"text\" \n            class=\"form-control\" \n            id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_phone-number-"
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "_number\" \n            name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_phone-number-"
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "_number\"\n            placeholder=\"555-555-5555\" value=\""
    + alias4(((helper = (helper = helpers.phoneNumber || (depth0 != null ? depth0.phoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"phoneNumber","hash":{},"data":data}) : helper)))
    + "\"\n            pattern=\"(\\+?[0-9]{0,3}[-\\.]?)?[1-9][0-9]{2}[-\\.]?[1-9][0-9]{2}[-\\.]?[0-9]{4}\"\n            oninvalid=\"this.setCustomValidity('Phone numbers must be digits only, and in the form 555-555-5555')\"\n            oninput=\"this.setCustomValidity('')\"\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.phoneRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n    </div>\n    <div class=\"col-sm-3\">\n        <select id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_phone-number-"
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "_type\" name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_phone-number-"
    + alias4(((helper = (helper = helpers.memberPhoneNumber || (depth0 != null ? depth0.memberPhoneNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberPhoneNumber","hash":{},"data":data}) : helper)))
    + "_type\" class=\"form-control\" value=\""
    + alias4(((helper = (helper = helpers.phoneType || (depth0 != null ? depth0.phoneType : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"phoneType","hash":{},"data":data}) : helper)))
    + "\" "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.phoneRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.phoneTypes : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </select>\n    </div>\n</div>";
},"useData":true}));
this["Phonebook"] = this["Phonebook"] || {};
this["Phonebook"]["templates"] = this["Phonebook"]["templates"] || {};
this["Phonebook"]["templates"]["family"] = this["Phonebook"]["templates"]["family"] || {};
this["Phonebook"]["templates"]["family"]["info"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "selected";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"form-group row align-items-center\">\n    <div class=\"col-sm-2\">\n        <label for=\"family-name\" class=\"col-form-label family-label\">Family Name</label>\n    </div>\n    <div class=\"col-sm-10\">\n        <input type=\"text\" class=\"form-control\" id=\"family-name\" name=\"family-name\" placeholder=\"Family Name\" value=\""
    + alias4(((helper = (helper = helpers.familyName || (depth0 != null ? depth0.familyName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyName","hash":{},"data":data}) : helper)))
    + "\" required>\n    </div>\n</div>\n<div class=\"form-group row align-items-center\">\n    <div class=\"col-sm-2\">\n        <label for=\"family-address-1\" class=\"col-form-label\">Address 1</label>\n    </div>\n    <div class=\"col-sm-10\">\n        <input type=\"text\" class=\"form-control\" id=\"family-address-1\" name=\"family-address-1\" placeholder=\"Address 1\" value=\""
    + alias4(((helper = (helper = helpers.familyAddress1 || (depth0 != null ? depth0.familyAddress1 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyAddress1","hash":{},"data":data}) : helper)))
    + "\">\n    </div>\n</div>\n<div class=\"form-group row align-items-center\">\n    <div class=\"col-sm-2\">\n        <label for=\"family-address-2\" class=\"col-form-label\">Address 2</label>\n    </div>\n    <div class=\"col-sm-10\">\n        <input type=\"text\" class=\"form-control\" id=\"family-address-2\" name=\"family-address-2\" placeholder=\"Address 2\" value=\""
    + alias4(((helper = (helper = helpers.familyAddress2 || (depth0 != null ? depth0.familyAddress2 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyAddress2","hash":{},"data":data}) : helper)))
    + "\">\n    </div>\n</div>\n<div class=\"form-group row align-items-center\">\n    <div class=\"col-sm-2\">\n        <label for=\"family-address-3\" class=\"col-form-label\">Address 3</label>\n    </div>\n    <div class=\"col-sm-10\">\n        <input type=\"text\" class=\"form-control\" id=\"family-address-3\" name=\"family-address-3\" placeholder=\"Address 3\" value=\""
    + alias4(((helper = (helper = helpers.familyAddress3 || (depth0 != null ? depth0.familyAddress3 : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyAddress3","hash":{},"data":data}) : helper)))
    + "\">\n    </div>\n</div>\n<div class=\"form-group row align-items-center\">\n    <div class=\"col-sm-2\">\n        <label for=\"family-city\" class=\"col-form-label\">City</label>\n    </div>\n    <div class=\"col-sm-3\">\n        <input type=\"text\" class=\"form-control\" id=\"family-city\" name=\"family-city\" placeholder=\"City\" value=\""
    + alias4(((helper = (helper = helpers.familyCity || (depth0 != null ? depth0.familyCity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyCity","hash":{},"data":data}) : helper)))
    + "\">\n    </div>\n    <div class=\"col-sm-1\">\n        <label for=\"family-province\" class=\"col-form-label\">Prov</label>\n    </div>\n    <div class=\"col-sm-2\">\n        <select id=\"family-province\" name=\"family-province\" class=\"form-control\" value=\""
    + alias4(((helper = (helper = helpers.familyProvince || (depth0 != null ? depth0.familyProvince : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyProvince","hash":{},"data":data}) : helper)))
    + "\">\n            <option value=\"NL\">NL</option>\n            <option value=\"PE\">PE</option>\n            <option value=\"NS\">NS</option>\n            <option value=\"NB\">NB</option>\n            <option value=\"QC\">QC</option>\n            <option value=\"ON\">ON</option>\n            <option value=\"MB\">MB</option>\n            <option value=\"SK\" "
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.province : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">SK</option>\n            <option value=\"AB\">AB</option>\n            <option value=\"BC\">BC</option>\n            <option value=\"YT\">YT</option>\n            <option value=\"NT\">NT</option>\n            <option value=\"NU\">NU</option>\n        </select>\n    </div>\n    <div class=\"col-sm-1\">\n        <label for=\"family-postal-code\" class=\"col-form-label\">PC</label>\n    </div>\n    <div class=\"col-sm-3\">\n        <input type=\"text\" class=\"form-control\" id=\"family-postal-code\" name=\"family-postal-code\" placeholder=\"H0H 0H0\" value=\""
    + alias4(((helper = (helper = helpers.familyPostalCode || (depth0 != null ? depth0.familyPostalCode : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"familyPostalCode","hash":{},"data":data}) : helper)))
    + "\" pattern=\"([a-zA-Z][0-9][a-zA-Z] ?[0-9][a-zA-Z][0-9])|([0-9]{5})\" oninvalid=\"this.setCustomValidity('Postal codes must either be in the Canadian [A1A 1A1] or American [90210]format')\" oninput=\"this.setCustomValidity('')\">\n    </div>\n</div>";
},"useData":true});
this["Phonebook"]["templates"]["family"]["member"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return " selected";
},"3":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "                    <option value=\""
    + alias2(alias1(depth0, depth0))
    + "\">"
    + alias2(alias1(depth0, depth0))
    + "</option>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"family-member-container\" data-member=\""
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "\">\n    <span class=\"member-number-display\" data-count=\""
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "</span>\n    <div class=\"member-info\">\n        <div class=\"form-group row align-items-center\">\n            <label for=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_first-name\" class=\"col-sm-2 col-form-label\">First Name</label>\n            <div class=\"col-sm-10\">\n                <input type=\"text\" class=\"form-control\" id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_first-name\" name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_first-name\" placeholder=\"First Name\" value=\""
    + alias4(((helper = (helper = helpers.memberFirstName || (depth0 != null ? depth0.memberFirstName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberFirstName","hash":{},"data":data}) : helper)))
    + "\" required>\n            </div>\n        </div>\n        <!-- <div class=\"form-group row align-items-center\">\n                <label for=\"member-1_first-name\" class=\"col-sm-2 col-form-label\">Middle Name</label>\n                <div class=\"col-sm-10\">\n                    <input type=\"text\" class=\"form-control\" id=\"member-1_middle-name\" name=\"member-1_middle-name\" placeholder=\"Middle Name\" value=\""
    + alias4(((helper = (helper = helpers.memberMiddleName || (depth0 != null ? depth0.memberMiddleName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberMiddleName","hash":{},"data":data}) : helper)))
    + "\">\n                </div>\n            </div> -->\n        <div class=\"form-group row align-items-center\">\n            <label for=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_last-name\" class=\"col-sm-2 col-form-label\">Last Name</label>\n            <div class=\"col-sm-10\">\n                <input type=\"text\" \n                    class=\"form-control\" \n                    id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_last-name\" \n                    name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_last-name\" \n                    value=\""
    + alias4(((helper = (helper = helpers.memberLastName || (depth0 != null ? depth0.memberLastName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberLastName","hash":{},"data":data}) : helper)))
    + "\"\n                    placeholder=\"Last Name\" \n                    data-different-from-main-last-name=\"false\"\n                    required>\n            </div>\n        </div>\n        <div class=\"member-phone-number-group\">\n"
    + ((stack1 = container.invokePartial(partials.memberPhoneNumberEntry,depth0,{"name":"memberPhoneNumberEntry","data":data,"indent":"            ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "        </div>\n        <div class=\"phone-number-controls\">\n            <span class=\"phone-number-control add\">(add phone number)</span>\n            <span class=\"phone-number-control remove\">(remove phone number)</span>\n        </div>\n        <div class=\"form-group row align-items-center\">\n            <label for=\"member-birthday\" class=\"col-sm-2 col-form-label\">Birthday</label>\n            <div class=\"col-sm-4\">\n                <select id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-month\" name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-month\" \n                  class=\"form-control form-month\" value=\""
    + alias4(((helper = (helper = helpers.memberBirthMonth || (depth0 != null ? depth0.memberBirthMonth : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberBirthMonth","hash":{},"data":data}) : helper)))
    + "\"\n                  data-birthday-pair=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-date\" required>\n                    <option value=\"1\""
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.memberBirthMonth : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">January</option>\n                    <option value=\"2\">February</option>\n                    <option value=\"3\">March</option>\n                    <option value=\"4\">April</option>\n                    <option value=\"5\">May</option>\n                    <option value=\"6\">June</option>\n                    <option value=\"7\">July</option>\n                    <option value=\"8\">August</option>\n                    <option value=\"9\">September</option>\n                    <option value=\"10\">October</option>\n                    <option value=\"11\">November</option>\n                    <option value=\"12\">December</option>\n                </select>\n            </div>\n            <div class=\"col-sm-2\">\n                <select id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-date\" name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-date\" \n                  class=\"form-control form-date\" value=\""
    + alias4(((helper = (helper = helpers.memberBirthDate || (depth0 != null ? depth0.memberBirthDate : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberBirthDate","hash":{},"data":data}) : helper)))
    + "\"\n                  data-birthday-pair=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-month\" required>\n                    <option value=\"1\">1</option>\n                    <option value=\"2\">2</option>\n                    <option value=\"3\">3</option>\n                    <option value=\"4\">4</option>\n                    <option value=\"5\">5</option>\n                    <option value=\"6\">6</option>\n                    <option value=\"7\">7</option>\n                    <option value=\"8\">8</option>\n                    <option value=\"9\">9</option>\n                    <option value=\"10\">10</option>\n                    <option value=\"11\">11</option>\n                    <option value=\"12\">12</option>\n                    <option value=\"13\">13</option>\n                    <option value=\"14\">14</option>\n                    <option value=\"15\">15</option>\n                    <option value=\"16\">16</option>\n                    <option value=\"17\">17</option>\n                    <option value=\"18\">18</option>\n                    <option value=\"19\">19</option>\n                    <option value=\"20\">20</option>\n                    <option value=\"21\">21</option>\n                    <option value=\"22\">22</option>\n                    <option value=\"23\">23</option>\n                    <option value=\"24\">24</option>\n                    <option value=\"25\">25</option>\n                    <option value=\"30\">30</option>\n                    <option value=\"26\">26</option>\n                    <option value=\"27\">27</option>\n                    <option value=\"28\">28</option>\n                    <option value=\"29\">29</option>\n                    <option value=\"30\">30</option>\n                    <option value=\"31\">31</option>\n                </select>\n            </div>\n            <div class=\"col-sm-4\">\n                <select id=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-year\" name=\"member-"
    + alias4(((helper = (helper = helpers.memberNumber || (depth0 != null ? depth0.memberNumber : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberNumber","hash":{},"data":data}) : helper)))
    + "_birthday-year\" \n                  class=\"form-control form-date\" value=\""
    + alias4(((helper = (helper = helpers.memberBirthYear || (depth0 != null ? depth0.memberBirthYear : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"memberBirthYear","hash":{},"data":data}) : helper)))
    + "\" required>\n                    <option value=\"no\">Prefer not to say</option>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.yearOptions : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </select>\n            </div>\n        </div>\n    </div>\n</div>";
},"usePartial":true,"useData":true});
this["Phonebook"]["templates"]["family"]["phoneNumber"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.memberPhoneNumberEntry,depth0,{"name":"memberPhoneNumberEntry","data":data,"helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"usePartial":true,"useData":true});