/* global Phonebook */
/* global PhoneTypes */
/* global YearOptions */

const debug = true;

//Controller for the Add Family form
//executes on load
const newFamilyForm = (function() {
    // custom console.log
    function debug_message(str, type='info') {
        if (debug) console[type](str);
    }    

    const familyInformationContainer = document.querySelector('.family-information');

    familyInformationContainer.innerHTML = Phonebook.templates.family.info({});

    // Declare function constants
    const familyNameInput = document.getElementById('family-name');
    const membersContainer = document.querySelector('.family-members-container');
    const membersControl = document.querySelector('.family-members-controls');
    const minMemberCount = 1;

    //create member forms, iterate and create the HTML from template
    for (let count = 1; count <= minMemberCount; count++) {
        const newHtmlContainer = document.createElement('div'); // create temporary holding element
        newHtmlContainer.innerHTML = Phonebook.templates.family.member({
            memberNumber: count,
            memberPhoneNumber: 1,
            phoneTypes: PhoneTypes,
            phoneRequired: false,
            yearOptions: YearOptions,
        });

        //append the elements to the container
        membersContainer.appendChild(newHtmlContainer.firstChild);
    }

    // create the listener on the container for adding more phone numbers if needed
    membersContainer.addEventListener('click', (event) => {
        // Helper function to get the member id and the phone number id of the last created
        function getLastPhoneNumberEntryData(container) {
            const lastPhoneNumberEntry = container.lastElementChild;

            return {
                member: parseInt(lastPhoneNumberEntry.dataset.member),
                phoneNumber: parseInt(lastPhoneNumberEntry.dataset.phoneNumber)
            };
        }

        // Helper function to add an entry to the member's form
        function addPhoneEntry(container) {
            // get the entry from the last phone number currently in the form
            const lastEntryData = getLastPhoneNumberEntryData(container);
            // create a temporary container
            const newEntryTempContainer = document.createElement('div');

            // create the elements for the new phone number
            newEntryTempContainer.innerHTML = Phonebook.templates.family.phoneNumber({
                memberNumber: lastEntryData.member,
                memberPhoneNumber: (lastEntryData.phoneNumber + 1),
                phoneTypes: PhoneTypes,
                phoneRequired: true,
            });

            // append the data to the appropriate area
            container.appendChild(newEntryTempContainer.firstChild);
        }

        function removePhoneEntry(container) {
            // get the entry from the last phone number currently in the form
            const lastEntryData = getLastPhoneNumberEntryData(container);

            // always leave one phone number entry
            if (lastEntryData.phoneNumber === 1) return;

            // remove the last phone number
            container.removeChild(container.lastElementChild);
        }

        // get the target element
        const target = event.target;

        // determine which button was clicked
        if (target.classList.contains('phone-number-control')) {
            if (target.classList.contains('add')) {
                // add the phone entry to the target's parent, previous sibling (e.g the phone number group for that member)
                addPhoneEntry(target.parentElement.previousElementSibling);
            } else if (target.classList.contains('remove')) {
                // remove the phone entry to the target's parent, previous sibling (e.g the phone number group for that member)
                removePhoneEntry(target.parentElement.previousElementSibling);
            }
        }
    });

    // add the listener for adding/removing members from the form
    membersControl.addEventListener('click', (event) => {
        // Helper function to get the last member added id
        function getLastMemberEntryData(container) {
            const lastMember = membersContainer.lastElementChild;

            return {
                memberNumber: parseInt(lastMember.dataset.member)
            };
        }

        // Helper function to add a member to the container
        function addMemberEntry(container) {
            const lastMemberData = getLastMemberEntryData(container);
            
            // create the elements in a temporary container
            const tempContainer = document.createElement('div');
            tempContainer.innerHTML = Phonebook.templates.family.member({
                yearOptions: YearOptions,
                memberNumber: (lastMemberData.memberNumber + 1),
                memberPhoneNumber: 1,
                phoneTypes: PhoneTypes,
                phoneRequired: false,
                lastName: familyNameInput.value,
            });

            // append the member
            container.appendChild(tempContainer.firstChild);
        }

        // Helper function to remove a member from the form
        function removeMemberEntry(container) {
            const lastEntryData = getLastMemberEntryData(container);

            if (lastEntryData.memberNumber === 1) return;

            container.removeChild(container.lastElementChild);
        }

        // get the target
        const target = event.target;

        // determine which button was clicked
        if (target.classList.contains('family-members-control')) {
            if (target.classList.contains('add')) {
                addMemberEntry(membersContainer);
            } else if (target.classList.contains('remove')) {
                removeMemberEntry(membersContainer);
            }
        }
    });

    membersContainer.addEventListener('change', (event) => {
        const target = event.target;
        
        if (/^member-[0-9]+_last-name$/.test(target.id)) {
            console.log(target);

            if (target.value.trim() === '') {
                target.value = familyNameInput.value;
                target.dataset.differentFromMainLastName = false;
            } else {
                target.dataset.differentFromMainLastName = true;
            }
        }
    });

    familyNameInput.addEventListener('input', () => {
        const lastNameInputs = document.querySelectorAll('[id$="last-name"][data-different-from-main-last-name="false"]');

        Array.from(lastNameInputs).forEach(lastNameInput => {
            lastNameInput.value = familyNameInput.value;
        });
    });
    
    // all done
    debug_message('Form set up.');
})();